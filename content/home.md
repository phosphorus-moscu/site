+++
title = "Welcome to Veloren!"
description = "A welcome to the Veloren development blog"

weight = 0
template = "404.html"
# Note: getting this content into the index page is a bit of a hack. Zola's live reloading will not detect changes.
# We render this with 404.html, because we do not want this to be accessible at /home/, just at /
+++

# Welcome to Veloren!

Veloren is a multiplayer voxel RPG written in Rust. It is inspired by games such
as Cube World, Legend of Zelda: Breath of the Wild, Dwarf Fortress and
Minecraft.

Veloren is fully open-source, licensed under GPL 3. It uses original graphics,
musics and other assets created by its community. Being contributor-driven, its
development community and user community is one and the same: developers,
players, artists and musicians come together to develop the game.

{% gallery() %}
0.jpg
1.jpg
2.jpg
3.jpg
4.jpg
5.jpg
6.jpg
7.jpg
8.jpg
9.png
10.png
{% end %}

Development of Veloren started in mid-2018. There are around 20 active
developers working on the game, with another 20 artists, writers, designers, and
composers contributing as well. Over 150 people have contributed to the
project. Veloren currently has builds for Windows, Linux, and MacOS.

<p style="text-align: center">What are you waiting for?</p>
