+++
title = "Join the Veloren community!"
description = "Join the Veloren community!"

weight = 0
+++

Veloren is open-source! Join us in making this game the best it can be.

* [Gitlab — Browse the source code](https://gitlab.com/veloren/veloren)
* [Discord — Join the discussion](https://discord.gg/ecUxc9N)
* [Matrix — Join the discussion](https://matrix.to/#/#veloren-space:fachschaften.org) | [(Room list)](@/matrix_room_list.md) | WIP
* [Reddit — Get the latest updates to this blog at r/Veloren](https://www.reddit.com/r/Veloren/)
* [Veloren Wiki — Contribute or read articles about every aspect of the game](https://wiki.veloren.net/)
